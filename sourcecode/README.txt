#setup for web app
export FLASK_ENV=development
export FLASK_APP=app.py

#development server
python -m flask run --host=0.0.0.0 --port 9131

#Use the following address to view the site when running on the development server
http://set09103.napier.ac.uk:9131
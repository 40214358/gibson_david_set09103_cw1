from flask import Flask, render_template, redirect, url_for
import json 
app = Flask(__name__)

with app.open_resource('static/content/content.json') as f:
    data = json.load(f)

@app.route('/')
def root():
    return render_template('home.html'), 200

@app.route("/home")
def home():
    return redirect(url_for('root'))

@app.route("/series")
def series():
    return render_template('series.html')

@app.route("/trailers")
def trailers():
    return render_template('trailers.html')

@app.route("/trailers/<key>")
def trailer(key):
    try: 
        title=str(data["Series"][key][0]["Title"])
        trailer=str(data["Series"][key][4]["Trailer"])
        description=str(data["Series"][key][5]["Description"])
        return render_template('trailer.html', key=key, title=title, trailer=trailer, description=description)
    except KeyError:
        return render_template('404.html'), 404

@app.errorhandler(404)
def page_not_found(error):
        return render_template('404.html'), 404

@app.route('/series/<key>')
def information(key):
    try: 
        title=str(data["Series"][key][0]["Title"])
        genre=str(data["Series"][key][1]["Genre"])
        cast=str(data["Series"][key][2]["Cast"])
        rating=str(data["Series"][key][3]["Rating"])
        description=str(data["Series"][key][5]["Description"])
        return render_template('information.html', key=key, title=title, genre=genre, cast=cast, rating=rating, trailer=trailer, description=description)
    except KeyError:
        return render_template('404.html'), 404

@app.route('/config/') 
def config():
    str = []
    str.append('Debug:'+app.config['DEBUG']) 
    str.append('port:'+app.config['port']) 
    str.append('url:'+app.config['url']) 
    str.append('ip_address:'+app.config['ip_address']) 
    return '\t'.join(str)

def init(app):
    config = ConfigParser.ConfigParser() 
    try:
        config_location = "etc/defaults.cfg" 
        config.read(config_location)
        app.config['DEBUG'] = config.get("config", "debug") 
        app.config['ip_address'] = config.get("config", "ip_address")
        app.config['port'] = config.get("config", "port") 
        app.config['url'] = config.get("config", "url")
    except:
        print("Could not read configs from: ", config_location)


if __name__ == "__main__":
    init(app)
    app.run(
        host=app.config['ip_address'],
        port=int(app.config['port']))
